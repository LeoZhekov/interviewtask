package egt.digital.interview.task.util;

import egt.digital.interview.task.dtos.FixerRatesResponseEntity;
import egt.digital.interview.task.dtos.json.JsonResponseDto;
import egt.digital.interview.task.dtos.xml.XmlCurrencyOut;
import egt.digital.interview.task.dtos.xml.XmlCurrencyRate;
import java.util.ArrayList;
import java.util.List;

/**
 * Converter util class for parsing the fixer response into appropriate objects for the clients.
 */
public class CurrencyConverter {

  public static XmlCurrencyOut parseXmlFromApi(FixerRatesResponseEntity entity) {
    List<XmlCurrencyRate> rates = new ArrayList<>();
    entity.getRates().forEach((cur, rate) -> rates.add(new XmlCurrencyRate(cur, rate)));
    return XmlCurrencyOut.builder()
        .currency(entity.getBase())
        .rates(rates)
        .date(entity.getDate().toString())
        .build();
  }

  public static JsonResponseDto parseJsonFromApi(FixerRatesResponseEntity entity) {
    return JsonResponseDto.builder()
        .currency(entity.getBase())
        .rates(entity.getRates())
        .date(entity.getDate())
        .build();
  }
}
