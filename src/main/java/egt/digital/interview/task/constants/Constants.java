package egt.digital.interview.task.constants;

/**
 * Constants class.
 */
public class Constants {
  public static final String EXT_SERVICE_1 = "EXT_SERVICE_1";
  public static final String EXT_SERVICE_2 = "EXT_SERVICE_2";
  public static final String RABBIT_TOPIC_EXCHANGE = "spring-boot-exchange";
  public static final String RABBIT_QUEUE_NAME = "spring-boot";
  public static final String RABBIT_BINDING_ROUTE = "binding.route.#";
}
