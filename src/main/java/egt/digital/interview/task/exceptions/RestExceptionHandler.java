package egt.digital.interview.task.exceptions;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * Exception handler to catch exceptions and throw some meaningful errors to the clients.
 */
@ControllerAdvice
@Slf4j
public class RestExceptionHandler {

  @ExceptionHandler(DuplicatedJSONRequestException.class)
  public ResponseEntity<ErrorResponse> handleDuplicatedJSONRequestException(final DuplicatedJSONRequestException exception) {
    log.error(exception.getMessage(), exception);
    return ResponseEntity.status(HttpStatus.CONFLICT)
        .body(new ErrorResponse(HttpStatus.CONFLICT.toString(), "Request already exists"));
  }

  @ExceptionHandler(DuplicatedXMLRequestException.class)
  public ResponseEntity<XMLErrorResponse> handleDuplicatedXMLRequestException(final DuplicatedXMLRequestException exception) {
    log.error(exception.getMessage(), exception);
    return ResponseEntity.status(HttpStatus.CONFLICT)
        .body(new XMLErrorResponse(HttpStatus.CONFLICT.toString(), "Request already exists"));
  }

  @ExceptionHandler(InvalidXMLArgumentException.class)
  public ResponseEntity<XMLErrorResponse> handleInvalidXMLArgumentException(final InvalidXMLArgumentException exception) {
    log.error(exception.getMessage(), exception);
    return ResponseEntity.status(HttpStatus.BAD_REQUEST)
        .body(new XMLErrorResponse(HttpStatus.BAD_REQUEST.toString(), "There are missing items in the XML body"));
  }
}
