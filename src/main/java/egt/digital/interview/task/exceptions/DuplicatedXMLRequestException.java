package egt.digital.interview.task.exceptions;

/**
 * Custom exception that is thrown when a request comes with a duplicate ID from an XML request.
 */
public class DuplicatedXMLRequestException extends RuntimeException {
}
