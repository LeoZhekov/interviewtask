package egt.digital.interview.task.exceptions;

/**
 * Custom exception that is thrown when a request comes with a duplicate ID from a JSON request.
 */
public class DuplicatedJSONRequestException extends RuntimeException {
}
