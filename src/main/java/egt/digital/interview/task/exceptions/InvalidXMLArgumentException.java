package egt.digital.interview.task.exceptions;

/**
 * Exception thrown when something is missing from the XML request body.
 */
public class InvalidXMLArgumentException extends RuntimeException {
}
