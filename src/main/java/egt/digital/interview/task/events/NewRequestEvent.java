package egt.digital.interview.task.events;

import egt.digital.interview.task.domain.entities.RequestEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * An event to be fired when a request has been made.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class NewRequestEvent {
  private RequestEntity requestEntity;
}
