package egt.digital.interview.task.events;

import egt.digital.interview.task.constants.Constants;
import egt.digital.interview.task.dtos.Receiver;
import java.util.concurrent.TimeUnit;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;

/**
 * Sender component that sends the message through RabbitMQ.
 */
@Component
@RequiredArgsConstructor
@Slf4j
public class MessageSender {
  private final RabbitTemplate rabbitTemplate;
  private final Receiver receiver;

  public void send(NewRequestEvent newRequestEvent) {
    try {
      System.out.println("Sending message...");
      rabbitTemplate.convertAndSend(Constants.RABBIT_TOPIC_EXCHANGE, Constants.RABBIT_BINDING_ROUTE + ".rab",
          newRequestEvent.getRequestEntity().toString());
      receiver.getLatch().await(10000, TimeUnit.MILLISECONDS);
    } catch (InterruptedException e) {
      log.error(e.getMessage(), e);
    }
  }
}
