package egt.digital.interview.task.events;

import lombok.RequiredArgsConstructor;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

/**
 * Event listener for the new request event. {@link NewRequestEvent}
 */
@Component
@RequiredArgsConstructor
public class RequestEventHandler {

  private final MessageSender messageSender;

  @Async
  @EventListener
  public void handleNewRequestEvent(NewRequestEvent newRequestEvent) {
    messageSender.send(newRequestEvent);
  }
}
