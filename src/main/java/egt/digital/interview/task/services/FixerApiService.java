package egt.digital.interview.task.services;

import egt.digital.interview.task.dtos.FixerRatesResponseEntity;
import java.time.LocalDate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * Service that calls the Fixer API with rest template.
 */
@Service
public class FixerApiService {
  private static final String BASE_PARAM = "&base=";
  private static final String SYMBOLS_PARAM_VALUE = "USD,AUD,CAD,PLN,MXN";
  private static final String START_DATE_PARAM = "&start_date=";
  private static final String END_DATE_PARAM = "&end_date";
  private static final String TIMESERIES_URL = "timeseries";
  private static final String LATEST_URL = "latest";
  private static final String ACCESS_KEY_PARAM = "?access_key=";
  private static final String SYMBOLS_PARAM = "&symbols=";
  private final RestTemplate restTemplate = new RestTemplate();

  @Value("${fixer.api-key}")
  private String apiKey;

  @Value("${fixer.base-url}")
  private String baseUrl;

  public FixerRatesResponseEntity findCurrent(String currency) {
    String urlForQuery = baseUrl + LATEST_URL + ACCESS_KEY_PARAM + apiKey + SYMBOLS_PARAM + currency;
    return restTemplate.getForEntity(urlForQuery, FixerRatesResponseEntity.class).getBody();
  }

  /**
   * Due to the fact that I have a free profile in fixer.io, I am unable to test this functionality as it is limited to higher tier (premiunm) accounts.
   * But it's supposed to be working. I checked the documentation and it's returning the same structure of json so mapping and all should be fine.
   */
  public FixerRatesResponseEntity getAllInPeriod(LocalDate from, LocalDate to, String currency) {
    String urlForQuery = baseUrl + TIMESERIES_URL +
        ACCESS_KEY_PARAM + apiKey + BASE_PARAM + currency + SYMBOLS_PARAM + SYMBOLS_PARAM_VALUE +
        START_DATE_PARAM + from.toString() + END_DATE_PARAM + to.toString();
    return restTemplate.getForEntity(urlForQuery, FixerRatesResponseEntity.class).getBody();
  }
}
