package egt.digital.interview.task.services;

import egt.digital.interview.task.constants.Constants;
import egt.digital.interview.task.domain.entities.RequestEntity;
import egt.digital.interview.task.domain.repositories.RequestRepository;
import egt.digital.interview.task.dtos.FixerRatesResponseEntity;
import egt.digital.interview.task.dtos.json.JsonBodyDto;
import egt.digital.interview.task.dtos.json.JsonResponseDto;
import egt.digital.interview.task.dtos.xml.Command;
import egt.digital.interview.task.dtos.xml.XmlCurrencyOut;
import egt.digital.interview.task.events.NewRequestEvent;
import egt.digital.interview.task.exceptions.DuplicatedJSONRequestException;
import egt.digital.interview.task.exceptions.DuplicatedXMLRequestException;
import egt.digital.interview.task.exceptions.InvalidXMLArgumentException;
import egt.digital.interview.task.util.CurrencyConverter;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import lombok.RequiredArgsConstructor;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;

/**
 * Service that handles parsing and saving of requests and calls fixer API service.
 */
@RequiredArgsConstructor
@Service
public class CurrencyService {
  private final FixerApiService fixerApi;
  private final RequestRepository requestRepository;
  private final ApplicationEventPublisher eventPublisher;

  public JsonResponseDto getCurrentJson(JsonBodyDto jsonBodyDto) {
    validateRequest(jsonBodyDto.getRequestId(), Constants.EXT_SERVICE_1);
    handleRequest(jsonBodyDto);
    return CurrencyConverter.parseJsonFromApi(fixerApi.findCurrent(jsonBodyDto.getCurrency()));
  }

  public JsonResponseDto getHistoryJson(JsonBodyDto jsonBodyDto) {
    validateRequest(jsonBodyDto.getRequestId(), Constants.EXT_SERVICE_1);
    handleRequest(jsonBodyDto);
    return CurrencyConverter.parseJsonFromApi(getAllInPeriod(
        jsonBodyDto.getPeriod(),
        jsonBodyDto.getCurrency()));
  }

  public XmlCurrencyOut executeCommand(Command xmlBody) {
    validateRequest(xmlBody.getId(), Constants.EXT_SERVICE_2);
    XmlCurrencyOut response;
    if (xmlBody.getGet() != null) {
      response = CurrencyConverter.parseXmlFromApi(fixerApi.findCurrent(xmlBody.getGet().getCurrency()));
    } else if (xmlBody.getHistory() != null) {
      response = CurrencyConverter.parseXmlFromApi(getAllInPeriod(
          xmlBody.getHistory().getPeriod(),
          xmlBody.getHistory().getCurrency()));
    } else {
      throw new InvalidXMLArgumentException();
    }
    handleRequest(xmlBody);
    return response;
  }

  private FixerRatesResponseEntity getAllInPeriod(Integer period, String currency) {
    ZonedDateTime periodEnd = ZonedDateTime.now(ZoneId.of("UTC"));
    ZonedDateTime periodStart = periodEnd.minusHours(period.longValue());
    return fixerApi.getAllInPeriod(periodStart.toLocalDate(), periodEnd.toLocalDate(), currency);
  }

  private void handleRequest(JsonBodyDto json) {
    RequestEntity requestEntity = RequestEntity.builder()
        .requestId(json.getRequestId())
        .timestamp(Timestamp.from(Instant.ofEpochMilli(json.getTimestamp())))
        .client(json.getClient())
        .service(Constants.EXT_SERVICE_1)
        .build();
    saveRequestAndPublishEvent(requestEntity);
  }

  private void handleRequest(Command xml) {
    RequestEntity requestEntity = RequestEntity.builder()
        .requestId(xml.getId())
        .service(Constants.EXT_SERVICE_2)
        .timestamp(Timestamp.from(ZonedDateTime.now(ZoneId.of("UTC")).toInstant()))
        .build();
    if (xml.getHistory() != null) {
      requestEntity.setClient(xml.getHistory().getConsumer());
    } else if (xml.getGet() != null) {
      requestEntity.setClient(xml.getGet().getConsumer());
    }
    saveRequestAndPublishEvent(requestEntity);
  }

  private void validateRequest(String requestId, String serviceName) {
    if (requestRepository.existsByRequestId(requestId)) {
      switch (serviceName) {
        case Constants.EXT_SERVICE_1:
          throw new DuplicatedJSONRequestException();
        case Constants.EXT_SERVICE_2:
          throw new DuplicatedXMLRequestException();
        default:
          throw new RuntimeException("invalid service name");
      }
    }
  }

  private void saveRequestAndPublishEvent(RequestEntity entity) {
    requestRepository.save(entity);
    eventPublisher.publishEvent(new NewRequestEvent(entity));
  }
}
