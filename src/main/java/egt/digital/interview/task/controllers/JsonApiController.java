package egt.digital.interview.task.controllers;

import egt.digital.interview.task.dtos.json.JsonBodyDto;
import egt.digital.interview.task.dtos.json.JsonResponseDto;
import egt.digital.interview.task.services.CurrencyService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Rest controller for the json api client.
 */
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/json_api", consumes = "application/json", produces = "application/json")
public class JsonApiController {

  private final CurrencyService currencyService;

  @PostMapping("/current")
  public ResponseEntity<JsonResponseDto> current(@RequestBody JsonBodyDto jsonBodyDto) {
    return ResponseEntity.ok(currencyService.getCurrentJson(jsonBodyDto));
  }

  @PostMapping("/history")
  public ResponseEntity<JsonResponseDto> history(@RequestBody JsonBodyDto jsonBodyDto) {
    return ResponseEntity.ok(currencyService.getHistoryJson(jsonBodyDto));
  }
}
