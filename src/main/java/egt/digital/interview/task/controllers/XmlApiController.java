package egt.digital.interview.task.controllers;


import egt.digital.interview.task.dtos.xml.Command;
import egt.digital.interview.task.dtos.xml.XmlCurrencyOut;
import egt.digital.interview.task.services.CurrencyService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Rest controller for the xml api client.
 */
@RequiredArgsConstructor
@RestController
@RequestMapping(value = "/xml_api", consumes = "application/xml", produces = "application/xml")
public class XmlApiController {

  private final CurrencyService currencyService;

  @PostMapping("/command")
  public XmlCurrencyOut executeCommand(@RequestBody Command xmlBody) {
    return currencyService.executeCommand(xmlBody);
  }

}
