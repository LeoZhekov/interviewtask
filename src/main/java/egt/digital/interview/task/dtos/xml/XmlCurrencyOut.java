package egt.digital.interview.task.dtos.xml;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

/**
 * XML POJO for response body of XML requests.
 * It uses different XML annotations because with the javax.* wasn't working for some reason and I didn't want to spend too much time
 * researching why and how. Currently it appears to output two nested tags '<rates></rates>' about which I have no idea why...
 */
@NoArgsConstructor
@Builder
@AllArgsConstructor
@JacksonXmlRootElement
public class XmlCurrencyOut {
  @JacksonXmlProperty
  private String currency;
  @JacksonXmlProperty
  private String date;
  @JacksonXmlProperty
  private List<XmlCurrencyRate> rates;
}
