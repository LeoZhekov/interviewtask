package egt.digital.interview.task.dtos.xml;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.Getter;
import lombok.Setter;

/**
 * A POJO for the XML request body. I managed to get this working as it is now with these names. I followed the documentation and it appeared
 * to be working. I haven't worked with XML bodies before.
 */
@Getter
@Setter
@XmlRootElement
public class Command {
  @XmlAttribute
  private String id;
  @XmlElement(nillable = true)
  private Get get;
  @XmlElement(nillable = true)
  private History history;
}
