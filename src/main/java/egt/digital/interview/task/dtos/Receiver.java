package egt.digital.interview.task.dtos;

import java.util.concurrent.CountDownLatch;
import org.springframework.stereotype.Component;

/**
 * A POJO for testing the RabbitMQ messages.
 */
@Component
public class Receiver {
  private final CountDownLatch latch = new CountDownLatch(1);

  public void handleMessage(String message) {
    System.out.println("Received <" + message + ">");
    latch.countDown();
  }

  public CountDownLatch getLatch() {
    return latch;
  }
}
