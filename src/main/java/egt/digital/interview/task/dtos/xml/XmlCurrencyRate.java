package egt.digital.interview.task.dtos.xml;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * XML POJO for the rates map response
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@XmlRootElement
public class XmlCurrencyRate {
  @XmlElement
  private String currency;
  @XmlElement
  private Double rate;
}
