package egt.digital.interview.task.dtos.xml;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.Getter;
import lombok.Setter;

/**
 * XML POJO for the history/period part.
 */
@Getter
@Setter
@XmlRootElement
public class History {
  @XmlAttribute
  private Long consumer;
  @XmlAttribute
  private String currency;
  @XmlAttribute
  private Integer period;
}
