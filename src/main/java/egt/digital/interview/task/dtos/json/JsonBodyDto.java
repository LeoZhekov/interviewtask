package egt.digital.interview.task.dtos.json;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * A POJO for the request body of the json api.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class JsonBodyDto {
  private String requestId;
  private Long timestamp;
  private Long client;
  private String currency;
  private Integer period;
}
