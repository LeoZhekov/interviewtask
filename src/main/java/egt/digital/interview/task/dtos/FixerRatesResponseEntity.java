package egt.digital.interview.task.dtos;

import java.time.LocalDate;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * A POJO for the response from the fixer.io API calls.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class FixerRatesResponseEntity {
  private boolean success;
  private long timestamp;
  private boolean historical;
  private String base;
  private LocalDate date;
  private Map<String, Double> rates;
}
