package egt.digital.interview.task.dtos.json;

import java.time.LocalDate;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * A POJO for the response of json requests.
 */
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class JsonResponseDto {
  private String currency;
  private Map<String, Double> rates;
  private LocalDate date;
}
