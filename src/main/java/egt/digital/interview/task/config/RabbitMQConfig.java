package egt.digital.interview.task.config;

import egt.digital.interview.task.dtos.Receiver;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


import static egt.digital.interview.task.constants.Constants.RABBIT_BINDING_ROUTE;
import static egt.digital.interview.task.constants.Constants.RABBIT_QUEUE_NAME;
import static egt.digital.interview.task.constants.Constants.RABBIT_TOPIC_EXCHANGE;

/**
 * Configuration bean for RabbitMQ connection.
 */
@Configuration
public class RabbitMQConfig {

  @Bean
  public Queue queue() {
    return new Queue(RABBIT_QUEUE_NAME, false);
  }

  @Bean
  public TopicExchange exchange() {
    return new TopicExchange(RABBIT_TOPIC_EXCHANGE);
  }

  @Bean
  public Binding binding(Queue queue, TopicExchange exchange) {
    return BindingBuilder.bind(queue).to(exchange).with(RABBIT_BINDING_ROUTE);
  }

  @Bean
  public SimpleMessageListenerContainer container(ConnectionFactory connectionFactory,
      MessageListenerAdapter listenerAdapter) {
    SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
    container.setConnectionFactory(connectionFactory);
    container.setQueueNames(RABBIT_QUEUE_NAME);
    container.setMessageListener(listenerAdapter);
    return container;
  }

  @Bean
  public MessageListenerAdapter listenerAdapter(Receiver receiver) {
    return new MessageListenerAdapter(receiver);
  }

}
