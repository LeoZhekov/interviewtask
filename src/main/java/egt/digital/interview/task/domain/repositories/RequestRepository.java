package egt.digital.interview.task.domain.repositories;

import egt.digital.interview.task.domain.entities.RequestEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Repository for the request to be saved in the db and check if existing.
 */
@Repository
public interface RequestRepository extends JpaRepository<RequestEntity, Long> {
  boolean existsByRequestId(String requestId);
}
