package egt.digital.interview.task.domain.entities;

import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Entity for the request data.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "request")
public class RequestEntity {

  @Id
  @Column(name = "id")
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "requestid")
  private String requestId;

  @Column(name = "timestamp")
  private Timestamp timestamp;

  @Column(name = "client")
  private Long client;

  @Column(name = "service")
  private String service;

  @Override
  public String toString() {
    return "{" +
        "requestId:'" + requestId + '\'' +
        ", timestamp:" + timestamp +
        ", client:" + client +
        ", service:'" + service + '\'' +
        '}';
  }
}
